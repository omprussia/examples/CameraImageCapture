# Camera Image Capture

This project is an example of getting a QImage from a video frame obtained from
camera video output. The frame is captured in the buffer.

Build status:
1. example - [![pipeline status](https://gitlab.com/omprussia/demos/CameraImageCapture/badges/example/pipeline.svg)](https://gitlab.com/omprussia/demos/CameraImageCapture/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/demos/CameraImageCapture/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/demos/CameraImageCapture/-/commits/dev)

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-CLAUSE.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a standard structure
of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.CameraImageCapture.pro](ru.auroraos.CameraImageCapture.pro)** file
  describes the project structure for the qmake build system.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[icons](qml/icons)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[CameraImageCapture.qml](qml/CameraImageCapture.qml)** file
    provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.CameraImageCapture.spec](rpm/ru.auroraos.CameraImageCapture.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.CameraImageCapture.desktop](ru.auroraos.CameraImageCapture.desktop)** file
  defines the display and parameters for launching the application.
  
## Compatibility

- example: the main branch corresponding to the OS version higher than 4.0.2.173

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
