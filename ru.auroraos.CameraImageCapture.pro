# SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.CameraImageCapture

QT += multimedia

CONFIG += \
    auroraapp

PKGCONFIG += \

SOURCES += \
    src/camerahelper.cpp \
    src/main.cpp \
    src/qmlimage.cpp

HEADERS += \
    src/camerahelper.h \
    src/qmlimage.h

DISTFILES += \
    rpm/ru.auroraos.CameraImageCapture.spec \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-CLAUSE.md \
    README.md \
    README.ru.md

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.CameraImageCapture.ts \
    translations/ru.auroraos.CameraImageCapture-ru.ts \

message(AURORA_SDK_VERSION: $$AURORA_SDK_VERSION)
DEFINES += \
    AURORA_SDK_VERSION=\\\"$${AURORA_SDK_VERSION}\\\"

