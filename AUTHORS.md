# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2023
* Dmitriy Lapshin, <d.lapshin@omp.ru>
  * Maintainer, 2023
  * Developer, 2023
* Evgeniy Samohin, <e.samohin@omp.ru>
  * Reviewer, 2023
  * Developer, 2023
* Andrey Tretyakov, <a.tretyakov@omp.ru>
  * Reviewer, 2023
* Oksana Torosyan, o.torosyan@omp.ru
  * Designer, 2024
