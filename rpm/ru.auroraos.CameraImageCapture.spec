Name:       ru.auroraos.CameraImageCapture

Summary:    Aurora OS Application CameraImageCapture
Version:    0.1
Release:    1
Group:      Qt/Qt
License:    BSD-3-Clause
URL:        https://auroraos.ru
Source0:    %{name}-%{version}.tar.bz2

# Aurora SDK version
%define aurora_sdk_version %(cat /etc/os-release | grep VERSION_ID | cut -d '=' -f2)

Requires:   sailfishsilica-qt5 >= 0.10.9
BuildRequires:  pkgconfig(auroraapp)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5Multimedia)


%description
This project provides an example of getting QImage from Video Frame is captured from
Camera VideoOutput to buffer.

%prep
%autosetup

%build
%qmake5 AURORA_SDK_VERSION=%{aurora_sdk_version}
%make_build

%install
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
