// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtQuick>

#include <auroraapp.h>

#include "camerahelper.h"
#include "qmlimage.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<QmlImage>("ru.auroraos.CameraImageCapture", 1, 0, "QmlImage");

    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("CameraImageCapture"));

    CameraHelper camera_helper;
    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->rootContext()->setContextProperty("cameraHelper", &camera_helper);
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/CameraImageCapture.qml")));
    view->show();

    return application->exec();
}
