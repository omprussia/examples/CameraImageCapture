// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import QtMultimedia 5.6
import Sailfish.Silica 1.0
import ru.auroraos.CameraImageCapture 1.0

Page {
    objectName: "mainPage"

    allowedOrientations: Orientation.Portrait

    PageHeader {
        id: pageHeader
        objectName: "pageHeader"

        title: qsTr("CameraImageCapture")
        extraContent.children: [
            IconButton {
                objectName: "aboutButton"
                icon.source: "image://theme/icon-m-about"
                anchors.verticalCenter: parent.verticalCenter

                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    Camera {
        id: camera
        objectName: "camera"

        captureMode: Camera.CaptureStillImage

        Component.onCompleted: cameraHelper.qcamera = camera
    }

    VideoOutput {
        objectName: "videoOutput"

        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        source: camera
        focus: visible // to receive focus and capture key events when visible

        MouseArea {
            anchors.fill: parent
            onClicked: cameraHelper.capture()
        }
    }

    QmlImage {
        objectName: "qmlImage"

        anchors {
            left: parent.left
            bottom: parent.bottom
        }

        width: Math.min(parent.width, parent.height) * 0.3
        height: Math.min(parent.width, parent.height) * 0.3

        Component.onCompleted: {
            cameraHelper.imageChanged.connect(function (newImage) {
                    setImage(newImage);
                });
        }
    }
}
