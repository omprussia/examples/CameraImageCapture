# Буферизованный кадр Камеры

Этот проект реализует пример получения QImage из буферизованного кадра, полученного с камеры.

Статус сборки:
1. example - [![pipeline status](https://gitlab.com/omprussia/demos/CameraImageCapture/badges/example/pipeline.svg)](https://gitlab.com/omprussia/demos/CameraImageCapture/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/demos/CameraImageCapture/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/demos/CameraImageCapture/-/commits/dev)

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-CLAUSE.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта

Проект имеет стандартную структуру приложения на базе C++ и QML для ОС Аврора.

* Файл **[ru.auroraos.CameraImageCapture.pro](ru.auroraos.CameraImageCapture.pro)**
 описывает структуру проекта для системы сборки qmake.
* Каталог **[icons](icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
* Каталог **[qml](qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
    * Каталог **[cover](qml/cover)** содержит реализации обложек приложения.
    * Каталог **[icons](qml/icons)** содержит дополнительные иконки интерфейса пользователя.
    * Каталог **[pages](qml/pages)** содержит страницы приложения.
    * Файл **[CameraImageCapture.qml](qml/CameraImageCapture.qml)**
  предоставляет реализацию окна приложения.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
    * Файл **[ru.auroraos.CameraImageCapture.spec](rpm/ru.auroraos.CameraImageCapture.spec)**
  используется инструментом rpmbuild.
* Каталог **[src](src)** содержит исходный код на C++.
    * Файл **[main.cpp](src/main.cpp)** является точкой входа в приложение.
* Каталог **[translations](translations)** содержит файлы перевода интерфейса пользователя.
* Файл **[ru.auroraos.CameraImageCapture.desktop](ru.auroraos.CameraImageCapture.desktop)**
 определяет отображение и параметры запуска приложения.
  
## Совместимость

- example: главная ветка, соответствующая версии ОС выше 4.0.2.173.

## Снимки экранов

![screenshots](screenshots/screenshots.png)

## This document in English

- [README.md](README.md)
